
require 'planet'

universe = {}
universe.cam = camera(vector(0,0), 1)

universe.planets = {}

function universe:init(size)
	for i = 1, size do
		-- todo randomize planet creation and move to planet class as init function
		--self.planets[i] = Planet:new(vector(0,0), 20, 10) 
		self.planets[i] = Planet(vector(0,0), 20, 10) 
	end
end

function universe:draw()
    self.cam:predraw()
	
	love.graphics.setColor(255,0,0,255)    
	for _, p in ipairs(universe.planets) do
		love.graphics.circle('fill', p.pos.x, p.pos.y, p.size, 100)
	end   
    
    self.cam:postdraw()
end

