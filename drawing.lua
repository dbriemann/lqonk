
require 'navigator'
require 'selector'
require 'universe'

function love.draw()
	love.graphics.setColor(255,255,255,255)
    love.graphics.print("FPS: "..love.timer.getFPS(), 10, 20)
    
    --draw world
    world_cam:predraw()
    --[[
    love.graphics.setColor(255,0,0,255)
    love.graphics.rectangle("line", -2600, -2600, 5200, 5200)
    --]]
    love.graphics.setColor(255,120,120,64)
    love.graphics.setLineWidth(2) 
    for radius=2500, 250, -250 do
        love.graphics.circle("line", 0, 0, radius, radius/2)
    end
    love.graphics.setLineWidth(1)
    love.graphics.line(-2500, 0, 2500, 0)
    love.graphics.line(0, -2500, 0, 2500)    
    --[[    
    
    love.graphics.setColor(50,50,50,255)
    love.graphics.rectangle("fill", -2500, -2500, 5000, 5000)    
    love.graphics.setColor(255,0,0,255)
    love.graphics.rectangle("line", -2500, -2500, 5000, 5000)
    love.graphics.circle("fill", 200, 200, 25, 30)
    love.graphics.setColor(255,255,255,255)
    love.graphics.circle("fill", 0, 0, 50, 30)
    --]]
    
    universe:draw()
    
    world_cam:postdraw()
    
    --draw hud elements
    navigator:draw()
    selector:draw()
end
