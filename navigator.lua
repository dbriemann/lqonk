
vector = require 'hump.vector'

navigator = {}
navigator.origin = vector(0,0)
navigator.target = vector(0,0)
navigator.is_active = false

function navigator:setTarget(v)
    if not self.is_active then
        self.origin = v:clone()
        self.is_active = true
    end
    self.target = v:clone()
end

function navigator:setInactive()
    self.is_active = false
    self.origin = vector(0,0)
    self.target = vector(0,0)
end

function navigator:getDir()
    -- todo remove magic number
    return (self.target - self.origin) / 20
end

function navigator:moveWorld() 
    if self.is_active then
        local zz = self:getDir() / world_cam.zoom
        world_cam:move(zz)
        world_cam:snapToScreen()
    end
end

function navigator:draw()
    if self.is_active then
        love.graphics.setColor(0,255,0,255)
        love.graphics.line(self.origin.x, self.origin.y, self.target.x, self.target.y)
        love.graphics.circle("fill", self.target.x, self.target.y, 5, 20)
        love.graphics.setColor(255,0,0,255)        
        love.graphics.circle("fill", self.origin.x, self.origin.y, 5, 20)   
    end
end
