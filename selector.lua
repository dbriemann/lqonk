
selector = {}
selector.c1 = vector(0,0)
selector.c2 = vector(0,0)
selector.is_active = false

function selector:setTarget(v)
    if not self.is_active then
        self.c1 = v:clone()
        self.is_active = true
    end
    self.c2 = v:clone()
end

function selector:setInactive()
    self.is_active = false
    self.c1 = vector(0,0)
    self.c2 = vector(0,0)
end

function selector:draw()
    if self.is_active then
        love.graphics.setColor(255, 255, 255, 30)
        local xsize = self.c2.x - self.c1.x
        local ysize = self.c2.y - self.c1.y
        love.graphics.rectangle("fill", self.c1.x, self.c1.y, xsize, ysize)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.rectangle("line", self.c1.x, self.c1.y, xsize, ysize) 
    end
end
