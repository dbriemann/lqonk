--[[
Copyright (c) 2010 Matthias Richter

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

Except as contained in this notice, the name(s) of the above copyright holders
shall not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
]]--

require 'mathext'

local _PATH = (...):match('^(.*[%./])[^%.%/]+$') or ''
local vector = vector or Vector or require(_PATH..'vector')

local camera = {}
camera.__index = camera

local function new(pos, zoom, rot)
	local pos  = pos or vector(love.graphics.getWidth(), love.graphics.getHeight()) / 2
	local zoom = zoom or 1
	local rot  = rot or 0
	return setmetatable({pos = pos, zoom = zoom, rot = rot}, camera)
end

function camera:rotate(phi)
	self.rot = self.rot + phi
end

function camera:translate(t)
	self.pos = self.pos + t  
end
camera.move = camera.translate

function camera:getMinZoom()
    local xzoom = love.graphics.getWidth() / ((self.bounds.r - self.bounds.l) * 1.05)
    local yzoom = love.graphics.getHeight() / ((self.bounds.b - self.bounds.t) * 1.05)
    return math.min(xzoom, yzoom)
end

function camera:zoomBy(z)
    local z = self.zoom * (z or 1)
    local minz = self:getMinZoom()
    if z < minz then
        self.zoom = minz
    else
        self.zoom = z
    end
end

function camera:zoomOut()
    local z = self:getMinZoom()
    self.zoom = z
    self:snapToScreen()
end

function camera:snapToScreen()
    if self.bounds then
        local hori = love.graphics.getWidth()
        local vert = love.graphics.getHeight()
        local sbtl = self:toCameraCoords(vector(self.bounds.l, self.bounds.t))
        local sbbr = self:toCameraCoords(vector(self.bounds.r, self.bounds.b))
        
        local dir = vector(0,0)
        
        if (sbtl.x > 0 or sbtl.y > 0 or sbbr.x < hori or sbbr.y < vert) then   
            if sbtl.x > 0 then
                dir.x = sbtl.x
            elseif sbbr.x < hori then
                dir.x = -(hori - sbbr.x)
            end
            
            if sbtl.y > 0 then
                dir.y = sbtl.y
            elseif sbbr.y < vert then
                dir.y = -(vert - sbbr.y)
            end
            
            dir = dir / self.zoom
        end        
        
        if (sbbr.x - sbtl.x < hori and sbbr.y - sbtl.y < vert) then
            dir = self.pos:permul(vector(-1, -1))            
        elseif (sbbr.x - sbtl.x < hori) then
            dir.x = -self.pos.x
        elseif (sbbr.y - sbtl.y < vert) then
            dir.y = -self.pos.y
        end
        
        self:move(dir)
    end
end

function camera:predraw()
	local center = vector(love.graphics.getWidth(), love.graphics.getHeight()) / (self.zoom * 2)
	love.graphics.push()
	love.graphics.scale(self.zoom)
	love.graphics.translate(center:unpack())
	love.graphics.rotate(self.rot)
	love.graphics.translate((-self.pos):unpack())
end

function camera:postdraw()
	love.graphics.pop()
end

function camera:draw(func)
	self:predraw()
	func()
	self:postdraw()
end

function camera:toCameraCoords(p)
	local w,h = love.graphics.getWidth(), love.graphics.getHeight()
	local p = (p - self.pos):rotate_inplace(self.rot)
	return vector(p.x * self.zoom + w/2, p.y * self.zoom + h/2)
end

function camera:toWorldCoords(p)
	local w,h = love.graphics.getWidth(), love.graphics.getHeight()
	local p = vector((p.x-w/2) / self.zoom, (p.y-h/2) / self.zoom):rotate_inplace(-self.rot)
	return p + self.pos
end

function camera:mousepos()
	return self:toWorldCoords(vector(love.mouse.getPosition()))
end

function camera:setBounds(l, r, t, b)
    self.bounds = { l=l, r=r, t=t, b=b }
end

function camera:getBounds() 
    return unpack(self.bounds)
end

-- the module
return setmetatable({new = new},
	{__call = function(_, ...) return new(...) end})
