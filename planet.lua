
Class = require 'hump.class'

Planet = Class{function(self, pos, size, rot_speed)
    self.pos = pos
    self.size = size
    self.rot_speed = rot_speed
    self.satellites = {}
    end
}

Satellite = Class{inherits = Planet
    
}

Ship = Class{function(self, pos)
    self.pos = pos
    end
}
