
camera = require 'hump.camera'
vector = require 'hump.vector'
world_cam = camera(vector(0,0), 1)

-- mouse press functions
-- catches mouse presses
function love.mousepressed(x, y, button)    
    local wc = world_cam:toWorldCoords(vector(x, y))
    
    --zoom in
    if button == "wu" then        
        world_cam:move(wc) --move point of interest to center
        world_cam:zoomBy(1.1) --zoom in
        local z = wc - world_cam:toWorldCoords(vector(x, y)) 
        world_cam:move(z) --move poi to last screen pos
        world_cam:snapToScreen()
    end
    
    --zoom out
    if button == "wd" then
        world_cam:move(wc) --move point of interest to center
        world_cam:zoomBy(0.9) --zoom in
        local z = wc - world_cam:toWorldCoords(vector(x, y)) 
        world_cam:move(z) --move poi to last screen pos
        world_cam:snapToScreen()
    end
end


function processDrags()
    if love.mouse.isDown("r") then
        local cc = vector(love.mouse.getPosition())
        navigator:setTarget(cc)
        world_cam:snapToScreen()
    elseif love.mouse.isDown("l") then
        local cc = vector(love.mouse.getPosition())
        selector:setTarget(cc)
    end
end


--catches mouse releases
function love.mousereleased(x, y, button)
    if button == "l" then 
        selector:setInactive()
    end
    if button == "r" then 
        navigator:setInactive()
    end
end

-- keyboard functions
-- catches keyboard presses
function love.keypressed(key)
    if key == "escape" then
        love.event.push("quit")
    end
    if key == "tab" then
        world_cam:zoomOut()
    end
end

-- catches keyboard releases
function love.keyreleased(key, unicode)
    if key == "tab" then
        -- TODO zoom out
    end
end
