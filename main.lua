
require 'logic'
require 'controls'
require 'drawing'

-- init function
-- init game variables, sprites, etc..
function love.load()

	--init game randoms
	universe:init(1)
	
	--[[
	--set fullscreen mode
    local modes = love.graphics.getModes()
    -- sort from smallest to largest
    table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
    print("Checking Mode " .. modes[1].width .. "x" .. modes[1].height .. " in window mode..")
    
    --for i,v in ipairs(modes) do print(i, v.width, v.height) end
    
    if love.graphics.checkMode(modes[1].width, modes[1].height, false, true) then
        print("Mode OK")
        love.graphics.setMode(modes[1].width, modes[1].height, false, true)
    else
        print("Mode NOT ok.. fallback")
    end
    --]]
    love.graphics.setLineStyle('smooth')
    love.graphics.setColor(255,255,255,255)
    love.graphics.setBackgroundColor(0,0,0)
    world_cam:setBounds(-2600, 2600, -2600, 2600)
    world_cam:zoomOut()
end

function love.focus(f)
  if not f then
    print("LOST FOCUS")
  else
    print("GAINED FOCUS")
  end
end

function love.quit()
  print("Thanks for playing! Come back soon!")
end
