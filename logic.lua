

-- update function
-- performs all engine updates
function love.update(dt)
    processDrags() -- process input drag commands
    navigator:moveWorld() -- move world dependent on drag
end
